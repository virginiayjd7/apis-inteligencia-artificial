/*
* @Author: Jean-Christoph Queval - Bourgeois
* @Date:   2017-02-05 21:09:05
* @Last Modified by:   Jean-Christoph Queval - Bourgeois
* @Last Modified time: 2017-02-05 22:18:33
*/

(function (window) {
	'use strict';

	var FUNC_TYPE = typeof function () {};
	window.HTML5Battery = HTML5Battery;

	function HTML5Battery() {

	}

	HTML5Battery.prototype.onLevelChange = onLevelChange;
	HTML5Battery.prototype.startTracking = startTracking;

	function startTracking(onLoaded) {
		this.batteryManager = null;
		if (typeof navigator.getBattery === FUNC_TYPE) {
			var self = this;
			navigator
				.getBattery()
				.then(function (battery) {
					self.batteryManager = battery;
					onLoaded(null);
				});
			
		}
		return onLoaded('err-not-supported');
	}

	function onLevelChange(onLevelUpdated) {
		var self = this;
		if (!this.batteryManager) {
			return onLevelUpdated('err-battery-not-tracked');
		}
		if (typeof onLevelUpdated != FUNC_TYPE) {
			return onLevelUpdated('err-no-function-provided');
		}
		this.batteryManager
			.addEventListener("chargingchange", function () {
				onLevelUpdated(null, self.batteryManager, self);
			});
		this.batteryManager
			.addEventListener("levelchange", function () {
				onLevelUpdated(null, self.batteryManager, self);
			});

		this.batteryManager
			.addEventListener('chargingtimechange', function() {
				onLevelUpdated(null, self.batteryManager, self);
			}); 

		this.batteryManager
			.addEventListener('dischargingtimechange', function() {
				onLevelUpdated(null, self.batteryManager, self);
			});
		onLevelUpdated(null, self.batteryManager, self);
	}

})(window);